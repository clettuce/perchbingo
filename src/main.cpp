//
//  main.cpp
//  perchBingo
//
//  Created by MORITA Atsushi on 11/2/14.
//
//

#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){
	ofSetupOpenGL(800,600,OF_WINDOW);			// <-------- setup the GL context
	srand(10);
	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(new ofApp());

}
