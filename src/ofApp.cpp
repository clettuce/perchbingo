//
//  ofApp.cpp
//  perchBingo
//
//  Created by MORITA Atsushi on 11/2/14.
//
//

#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
	ofSetVerticalSync(true);
	ofBackground(0, 0, 0);
	ofSetLogLevel(OF_LOG_NOTICE);
	
	box2d.init();
	box2d.setGravity(0.3f, 10.f);
	box2d.createBounds();
	box2d.setFPS(30.0);
	box2d.registerGrabbing();
	
	textColor.set(255.f, 255.f, 255.f);
	
	font.loadFont("questrial.ttf", 80);
	image[0].loadImage("output/a.png");
	image[1].loadImage("output/b.png");
	image[2].loadImage("output/c.png");
	image[3].loadImage("output/d.png");
	image[4].loadImage("output/e.png");
	image[5].loadImage("output/f.png");
	image[6].loadImage("output/g.png");
	image[7].loadImage("output/h.png");
	image[9].loadImage("output/bg.png");
	
	
	imageDrawFrag = 0;
	showGiftList = false;
	for (int i = 0; i < 10; i++)
		showGiftListDelLine[i] = false;
	
	giftListX = 0.0f;
	giftListY = 0.0f;
	
	bgImage.loadImage("bg1.jpg");
	
	// SVG Gift List
	giftList.load("svg/giftlist.svg");
	giftDelLine.load("svg/giftdelline.svg");
}

//--------------------------------------------------------------
void ofApp::update(){
	box2d.update();
	for(int i=0; i<circles.size(); i++) {
		ofFill();
		circles[i].get()->update();
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	int width = ofGetViewportWidth();
	int height = ofGetViewportHeight();
	
	if (!imageDrawFrag && !showGiftList) {
		bgImage.draw(ofRectangle(0, 0, width, height));
	}
	
	if (!imageDrawFrag && !showGiftList) {
		if (!circles.empty() && imageDrawFrag == 0) {
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			ofSetColor(0.f, 0.f, 0.f, 127.f);	// alpha blend?
			ofRect(width / 4 * 3, 0, width / 4, 105);
			ofSetColor(255.f, 255.f, 255.f, 255.f);
			float scale = 1.f;
			glPushMatrix();
			glScalef(scale, scale, 1.f);
			ofSetColor(textColor);
			int text_length = text.length() > 0 ? text.length() : circles.back().get()->innerStr.length();
			font.drawString(text.length() > 0 ? text : circles.back().get()->innerStr,
					width / scale - font.getSize() * text_length * 0.7 - 30, font.getSize() + 10);
			glPopMatrix();
		}
		
		for(int i=0; i<circles.size(); i++) {
			circles[i].get()->drawFont(&font);
		}
	}
	if (imageDrawFrag ) {
		glDisable(GL_BLEND);
		image[9].draw(0, 0, width, height);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		image[imageDrawFrag - 'A'].draw(0, 0, width, height);
		glDisable(GL_BLEND);
	}

	
	if (showGiftList && !imageDrawFrag) {
		float giftScaleW = (float)width / 1600.f;
		float giftScaleH = (float)height / 1200.f;
		
		image[9].draw(ofRectangle(0, 0, width, height));
		
		glPushMatrix();
		glScalef(giftScaleW, giftScaleH, 1.f);
		glTranslatef(giftListX, giftListY, 0);
		giftList.draw();
		glPopMatrix();
		
		for (int i = 0; i < 10; i++) {
			if (showGiftListDelLine[i]) {
				glPushMatrix();
				glScalef(giftScaleW, giftScaleH, 1.f);
				glTranslatef(70.f + giftListX, 132.f + (float)(119*i) + giftListY, 0);
				giftDelLine.draw();
				glPopMatrix();
			}
		}
	}
	
	

#ifdef _DEBUG
	string info = "";
	info += "\n\nFPS: "+ofToString(ofGetFrameRate());
	info += "\nNumber of circles: " + ofToString(circles.size(), 0);
	ofDrawBitmapString(info, 20, 30);
#endif
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
	bool key_input_done = false;
	
	// ランダム数字push
	if (key == 'r')
	{
		text.clear();
		int random = ofRandom(0, 120);
		if (random >= 100)
			text.push_back('0' + (char)(random / 100));
		if (random >= 10)
			text.push_back('0' + (char)((random / 10) % 10));
		text.push_back('0' + (char)(random % 10));
		
		key_input_done = true;
	}

	// 数字をpush
	if (key >= '0' && key <= '9')
	{
		if (showGiftList) {
			showGiftListDelLine[key-'0'] = !showGiftListDelLine[key-'0'];
		}
		else if (text.length() < 3) {
			text.push_back(key);
		}
	}

	// 数字をpop
	if (text.length() == 3 || (text.length() > 0 && key == OF_KEY_RETURN) || key_input_done)
	{
		float r = 60;
		int x = ofRandom(font.getSize()/2, 300);
		int y = ofRandom(10, 200);
		
		circles.push_back(ofPtr<TexturedCircle>(new TexturedCircle));
		circles.back().get()->setPhysics(3.0, 0.53, 0.1);
		circles.back().get()->setup(box2d.getWorld(), x, y, r);
		
		circles.back().get()->innerStr = text;
		if (isSpecial) {
			circles.back().get()->setSpecial();
			isSpecial = false;
		}
		
		text.clear();
	}
	
	// 画像の全画面表示
	if (key >= 'A' && key <= 'J') {
		if (imageDrawFrag != key)
			imageDrawFrag = key;
		else
			imageDrawFrag = 0;	// same key -> no pic
	}
	if (key == 'Z') {
		imageDrawFrag = 0;
	}

	switch (key) {
		// 背景リロード
		case OF_KEY_F1:
			bgImage.loadImage("bg1.jpg");
			break;
		case OF_KEY_F2:
			bgImage.loadImage("bg2.jpg");
			break;
		case OF_KEY_F3:
			bgImage.loadImage("bg3.jpg");
			break;
		case OF_KEY_F4:
			bgImage.loadImage("bg4.jpg");
			break;
		case OF_KEY_F5:
			bgImage.loadImage("bg5.jpg");
			break;
			
		// 景品リストの移動
		case OF_KEY_UP:
			if (showGiftList)
				giftListY -= 10.f;
			break;
		case OF_KEY_DOWN:
			if (showGiftList)
				giftListY += 10.f;
			break;
		case OF_KEY_RIGHT:
			if (showGiftList)
				giftListX += 10.f;
			break;
		case OF_KEY_LEFT:
			if (showGiftList)
				giftListX -= 10.f;
			break;
			
		// 文字色変更
		case 'T':
		case 't':
			// change text color
			if (textColor.r == 255 && textColor.g == 255)
				textColor.set(0.f, 0.f, 0.f);
			else if (textColor.r == 0 && textColor.g == 0)
				textColor.set(255.f, 0.f, 0.f);
			else if (textColor.r == 255 && textColor.g == 0)
				textColor.set(0.f, 190.f, 255.f);
			else
				textColor.set(255.f, 255.f, 255.f);
			break;
			
		// フルスクリーン切り替え
		case 'f':
			ofToggleFullscreen();
			break;
			
		// sphere扱う
		case OF_KEY_DEL:
			// 最後に追加したSphereを削除
			if (circles.size() > 0) {
				circles.pop_back();
			}
			break;
		case OF_KEY_BACKSPACE:
			// 最後に追加した文字を削除
			if (!text.empty()) {
				text.erase(text.length()-1);
			}
			break;
		case 'd':
			// 最初に追加したSphereを削除
			if (!circles.empty()) {
				circles.erase(circles.begin());
			}
			break;
		case 'g':
			box2d.setGravity(-10.f, -5.f);
			break;
		case 'j':
			box2d.setGravity(-2.f, 10.f);
			break;
		case 'k':
			box2d.setGravity(8.f, -7.f);
			break;
		case 'h':	// default
			box2d.setGravity(0.3f, 10.f);
			break;
			
		case 'l':
			showGiftList = !showGiftList;
			break;
			
		case 'S':
			isSpecial = !isSpecial;
			break;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
