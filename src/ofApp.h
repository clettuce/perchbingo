//
//  ofApp.h
//  perchBingo
//
//  Created by MORITA Atsushi on 11/2/14.
//
//

#pragma once

#include "ofMain.h"
#include "texturedCircle.h"
#include "ofxSvg.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
	
		ofxBox2d box2d;
		vector <ofPtr<TexturedCircle> > circles;		  //	default box2d circles
	
		ofTrueTypeFont font;
	
		ofImage bgImage;
		ofImage image[10];
	
		ofColor textColor;
		string text;	// temporary input
	
		bool showGiftList;
		bool showGiftListDelLine[10];
		bool giftListInputMode;
		ofxSVG giftList;
		ofxSVG giftDelLine;
		
		float giftListX;
		float giftListY;
		
		bool isSpecial;
	
		char imageDrawFrag;
};
