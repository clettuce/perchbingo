//
//  texturedCircle.cpp
//  perchBingo
//
//  Created by MORITA Atsushi on 11/2/14.
//
//

#include "texturedCircle.h"

TexturedCircle::TexturedCircle()
{
	innerStr.clear();
	drawCount = 0;
	isSpecial = false;
	
	switch (rand() % 5)
	{
		case 0:
			fillColor.set(253, 74, 7, 255);
			break;
		case 1:
			fillColor.set(100, 66, 53, 255);
			break;
		case 2:
			fillColor.set(202, 26, 38, 255);
			break;
		case 3:
			fillColor.set(50, 209, 42, 255);
			break;
		case 4:
			fillColor.set(100, 253, 0, 255);
			break;
	}
}

void TexturedCircle::setSpecial()
{
	isSpecial = true;
	/* todo */
}

void TexturedCircle::draw()
{
	float radius = getRadius();
	
	glPushMatrix();
	
	glTranslatef(getPosition().x, getPosition().y, 0);
	
	ofFill();
	ofSetColor(31, 127, 255, 255);
	ofCircle(0, 0, radius);
	
	glPopMatrix();
}

void TexturedCircle::drawFont(ofTrueTypeFont *font)
{
	float circleScale = 1.0f;
	float font_circle_ratio = 0.54;
	
	const int anime_begin = 300;
	const int anime_end = 500;
	float angle;
	
	if (drawCount == 0)
	{
		firstRadius = getRadius();
	}
		
	// radius
	if (drawCount < anime_begin) {
		angle = getRotation();
		drawCount++;
	}
	else if (drawCount == anime_begin) {
		angle = getRotation();
		animStartAngle = angle - (float)(((int)(angle + 180) / 360) * 360);
		drawCount++;
	}
	else if (drawCount > anime_end) {
		circleScale = 0.5f;
		angle = 0.0f;
	}
	else { //if (drawCount <= anime_end && drawCount > anime_begin) {
		circleScale = (float)((anime_end-anime_begin)*2-(drawCount-anime_begin))/((anime_end-anime_begin)*2);
		if (!isSpecial)
			setRadius(firstRadius * circleScale);
		angle = animStartAngle * (float)(anime_end - drawCount) / (float)(anime_end - anime_begin);
		drawCount++;
	}
	
	if (isSpecial) {
		circleScale = 1.f;
	}
	
	glPushMatrix();
	
	glTranslatef(getPosition().x, getPosition().y, 0);
	glRotatef(angle, 0.f, 0.f, 1.f);
	
	// circle
	ofFill();
	ofSetColor(fillColor);
	ofCircle(0, 0, getRadius());

	// letter
	ofSetColor(255, 255, 255, 255);
	int fontsize = font->getSize();
	int xpos;
	switch (innerStr.size()) {
		default:
		case 1: xpos = -fontsize/3; break;
		case 2: xpos = -fontsize * 2/3; break;
		case 3: xpos = -fontsize * 1.1; break;
	}
	
	glScalef(circleScale * font_circle_ratio, circleScale * font_circle_ratio, 1.0);
	
	font->drawString(innerStr, xpos, fontsize/2);
	
	glPopMatrix();
}
