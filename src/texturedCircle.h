//
//  texturedCircle.h
//  perchBingo
//
//  Created by MORITA Atsushi on 11/2/14.
//
//

#ifndef __clettuceQuiz__texturedCircle__
#define __clettuceQuiz__texturedCircle__

#include "ofxBox2d.h"

class TexturedCircle : public ofxBox2dCircle {
	public:
		TexturedCircle();
		void draw();
		void drawFont(ofTrueTypeFont *font);
		void setSpecial();
		string innerStr;
		int drawCount;
	
private:
		float firstRadius;
		float animStartAngle;
		ofColor fillColor;
		bool isSpecial;
};

#endif /* defined(__clettuceQuiz__texturedCircle__) */
